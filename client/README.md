## My own sandbox

This is the pet project based on React / Redux, TypeScript will be soon.

Material design with Bootstrap 4 https://mdbootstrap.com/

API - https://developers.themoviedb.org/