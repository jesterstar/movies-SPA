import * as types from './actionTypes';
import ApiService from './../services/api';

const getPopularMoviesRequest = () => {
  return {
    type: types.FETCH_POPULAR_MOVIES_REQUEST
  };
};

const getPopularMoviesSuccess = (payload) => {
  return {
    type: types.FETCH_POPULAR_MOVIES_SUCCESS,
    payload
  };
};

const getPopularMoviesError = (error) => {
  return {
    type: types.FETCH_POPULAR_MOVIES_ERROR
  };
};

export const getPopularMoviesWithRedux = () => (dispatch) => {
  dispatch(getPopularMoviesRequest());
  return ApiService.getPopularMovies()
    .then(payload => dispatch(getPopularMoviesSuccess(payload)))
    .catch(error => dispatch(getPopularMoviesError(error)));
};