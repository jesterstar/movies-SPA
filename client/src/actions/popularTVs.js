import * as types from './actionTypes';
import ApiService from './../services/api';

const getPopularTVsRequest = () => {
  return {
    type: types.FETCH_POPULAR_TVS_REQUEST
  };
};

const getPopularTVsSuccess = (payload) => {
  return {
    type: types.FETCH_POPULAR_TVS_SUCCESS,
    payload
  };
};

const getPopularTVsError = (error) => {
  return {
    type: types.FETCH_POPULAR_TVS_ERROR
  };
};

export const getPopularTVsWithRedux = () => (dispatch) => {
  dispatch(getPopularTVsRequest());
  return ApiService.getPopularTVs()
    .then(payload => dispatch(getPopularTVsSuccess(payload)))
    .catch(error => dispatch(getPopularTVsError(error)));
};