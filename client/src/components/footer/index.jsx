import React from 'react';
import { connect } from 'react-redux';

import {
  Col,
  Row,
  Container
} from 'mdbreact';
import { ToastContainer } from 'react-toastify';

import './style.css';

export class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <Container>
          <Row>
            <Col xs="6" className="text-left">
              <a href="#">
                About Us              
              </a>
              <a href="#">
                Celebrities
              </a>
              <a href="#">
                News
              </a>
              <a href="#">
                Contacts
              </a>
            </Col>
            <Col xs="6" className="text-right">
              <span>CopyRight © 2018 Design by <a href="#">Me</a>. All Rights Reserved.</span>
            </Col>
          </Row>
        </Container>
        <ToastContainer
          position="bottom-right"
          autoClose={3000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick={true}
          pauseOnHover={true}
        />
      </footer>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  dispatch => ({})
)(Footer);