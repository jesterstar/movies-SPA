import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import {
  Navbar, 
  NavbarBrand, 
  NavbarNav, 
  NavbarToggler, 
  Collapse, 
  NavItem, 
  NavLink,
  Col,
  Row,
  Container,
  Button
} from 'mdbreact';

import './style.css';

export class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      isWideEnough: false
    };

    this.onClick = this.onClick.bind(this);
  }

  onClick(){
    this.setState({
      collapse: !this.state.collapse,
    });
  }

  render() {
    return (      
      <Router>
        <div className="header">
          <Container className="header__top">
            <Row>
              <Col>          
                <Navbar color="transparent" dark expand="md" scrolling>
                  <NavbarNav left className="navbar-user">
                    <NavItem>
                      <NavLink to="#">
                        <i className="zmdi zmdi-account-circle" />
                        Register
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink to="#">
                        <i className="zmdi zmdi-lock" />
                        Login
                      </NavLink>
                    </NavItem>
                  </NavbarNav>                
                  <NavbarNav>
                    <NavItem>
                      <NavLink to="#">
                        <i className="zmdi zmdi-email" />
                        Superhit Top Movie <span>**KING STAR**</span>
                      </NavLink>
                    </NavItem>
                  </NavbarNav>
                  <NavbarNav right>                  
                    <NavItem>
                      <i className="zmdi zmdi-accounts" />
                      Day visitor 32155
                    </NavItem>
                  </NavbarNav>
                </Navbar>
              </Col>
            </Row>            
          </Container>
          <hr/>
          <Container className="header__bottom">
            <Row>
              <Col>          
                <Navbar color="transparent" dark expand="md" scrolling>
                  <NavbarBrand href="/">
                    <strong>Movie <span>SPA</span></strong>
                  </NavbarBrand>
                  { !this.state.isWideEnough && <NavbarToggler onClick = { this.onClick } />}
                  <Collapse isOpen = { this.state.collapse } navbar>
                    <NavbarNav>
                      <NavItem active>
                        <NavLink to="#">Home</NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink to="#">Movie Layout</NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink to="#">Celebrities</NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink to="#">Showtime</NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink to="#">News</NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink to="#">Contacts</NavLink>
                      </NavItem>
                    </NavbarNav>
                    <NavbarNav right>
                      <NavItem>
                        <Button color="warning" rounded>Search</Button>
                      </NavItem>
                    </NavbarNav>
                  </Collapse>
                </Navbar>
              </Col>
            </Row>
          </Container>
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(
  mapStateToProps,
  dispatch => ({})
)(Header);