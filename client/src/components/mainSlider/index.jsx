import React from 'react';
import { connect } from 'react-redux';

import Slider from 'react-slick';

import './style.css';

export class MainSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      settings: {
        dots: false,
        infinite: true,
        autoplay: true,
        speed: 2500,
        slidesToShow: 1,
        slidesToScroll: 1,
        className: 'main-slider custom-slick'
      }
    };
  }

  render() {
    let test1 = {
      'backgroundImage': 'url(\'https://www.pixelstalk.net/wp-content/uploads/2016/04/Avengers-wallpapers-HD-free-download.jpg\')'
    };
    let test2 = {
      'backgroundImage': 'url(\'http://s1.1zoom.me/big0/182/Zoe_Saldana_Warriors_Men_436251.jpg\''
    };

    return (
      <Slider {...this.state.settings}>
        <div>
          <div className="image" style={test1}></div>
        </div>
        <div>
          <div className="image" style={test2}></div>
        </div>
      </Slider>
    );
  }
}

// function mapStateToProps() {
//   return
// }

export default connect(
  // mapStateToProps,
  // dispatch
)(MainSlider);