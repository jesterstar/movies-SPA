import React from 'react';
import { connect } from 'react-redux';

import Slider from 'react-slick';
import GeneralHelper from './../../helpers/general';
import { getPopularMoviesWithRedux } from './../../actions/popularMovies';

import { config } from './../../configs';

import './style.css';

export class PopularMovies extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isComponentLoaded: true,
      loaderDelay: 2000,
      popularMovies: this.props.popularMovies,
      settings: {
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: true,
        swipeToSlide: true,
        centerPadding: '0',
        className: 'custom-slick popular-slider'
      }
    };
  }

  componentDidMount() {
    this.props.onGetPopularMoviesWithRedux();
  }

  slidesToRender(slides) {
    return slides.map((slide, index) => {
      if (!slide.backdrop_path) return;

      let testStyle = {
        'backgroundImage': `url(${config.api.img.big + slide.backdrop_path})`
      };      

      return (
        <div key={slide.id + index} className="slide-item" data-id={slide.id}>
          <div className="details">
            <span className="duration">duration</span>
            <span className="vote"><i className="zmdi zmdi-star-outline" /> {slide.vote_average} / 10</span>
            <div>
              <p className="title">{slide.title}</p>
              <p className="description">{slide.overview}</p>
              <p className="release">RELEASE: {GeneralHelper.defalutDataFormat(slide.release_date)}</p>
            </div>
            <span className="details">details</span>
            <span className="views">views</span>
          </div>
          <div className="image z-depth-2" style={testStyle} alt={slide.title}/>
        </div>
      );
    });
  }

  render() {
    if (!this.props.popularMovies) {
      return <div className="loader small" />;
    } else {
      const slides = this.props.popularMovies.results;

      setTimeout(() => {
        this.setState({
          isComponentLoaded: false
        });
      }, this.state.loaderDelay);

      return (
        <div>
          <div className={this.state.isComponentLoaded ? 'loader small' : 'loader loaded'} />
          {!this.state.isComponentLoaded ? <h2>Popular movies <small>The most popular movies</small></h2> : ''}
          {!this.state.isComponentLoaded ? <Slider {...this.state.settings}>
            {this.slidesToRender(slides)}
          </Slider> : ''}
        </div>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    popularMovies: state.getPopularMoviesWithRedux.popularMovies
  };
}

export default connect(
  mapStateToProps,
  dispatch => ({
    onGetPopularMoviesWithRedux: () => {
      dispatch(getPopularMoviesWithRedux());
    }
  })
)(PopularMovies);