import React from 'react';
import { connect } from 'react-redux';

import Slider from 'react-slick';
import GeneralHelper from './../../helpers/general';
import { getPopularTVsWithRedux } from './../../actions/popularTVs';

import { config } from './../../configs';

import './style.css';

export class PopularTVs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isComponentLoaded: true,
      loaderDelay: 2000,
      popularTVs: this.props.popularTVs,
      settings: {
        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        swipeToSlide: true,
        beforeChange: function(currentSlide, nextSlide) {
          console.log("before change", currentSlide, nextSlide);
        },
        afterChange: function(currentSlide) {
          console.log("after change", currentSlide);
        },
        className: 'custom-slick popular-slider'
      }
    };
  }

  componentDidMount() {
    this.props.onGetPopularTVsWithRedux();
  }

  slidesToRender(slides) {
    return slides.map((slide, index) => {
      if (!slide.backdrop_path) return;

      let testStyle = {
        'backgroundImage': `url(${config.api.img.big + slide.backdrop_path})`
      };      

      return (
        <div key={slide.id + index} className="slide-item" data-id={slide.id}>
          <div className="details">
            <span className="duration">duration</span>
            <span className="vote"><i className="zmdi zmdi-star-outline" /> {slide.vote_average} / 10</span>
            <div>
              <p className="title">{slide.title}</p>
              <p className="description">{slide.overview}</p>
              <p className="release">RELEASE: {GeneralHelper.defalutDataFormat(slide.release_date)}</p>
            </div>
            <span className="details">details</span>
            <span className="views">views</span>
          </div>
          <div className="image z-depth-2" style={testStyle} alt={slide.title}/>
        </div>
      );
    });
  }

  render() {
    if (!this.props.popularTVs) {
      return <div className="loader small" />;
    } else {
      const slides = this.props.popularTVs.results;

      setTimeout(() => {
        this.setState({
          isComponentLoaded: false
        });
      }, this.state.loaderDelay);

      return (
        <div>
          <div className={this.state.isComponentLoaded ? 'loader small' : 'loader loaded'} />
          {!this.state.isComponentLoaded ? <h2>Popular TVs <small>The most popular TVs</small></h2> : ''}
          {!this.state.isComponentLoaded ? <Slider {...this.state.settings}>
            {this.slidesToRender(slides)}
          </Slider> : ''}
        </div>
      );
    }
  }
}

function mapStateToProps(state) {
  return {
    popularTVs: state.getPopularTVsWithRedux.popularTVs
  };
}

export default connect(
  mapStateToProps,
  dispatch => ({
    onGetPopularTVsWithRedux: () => {
      dispatch(getPopularTVsWithRedux());
    }
  })
)(PopularTVs);