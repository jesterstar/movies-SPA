export const api = {
  url: process.env.REACT_APP_API_URL,
  version: process.env.REACT_APP_API_VERSION,
  key: process.env.REACT_APP_API_KEY,
  lang: process.env.REACT_APP_API_LANG,
  img: {
    big: 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'
  },
  popular: {
    movies:'/movie/popular',
    tv: '/tv/popular'
  }
};