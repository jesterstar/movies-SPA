import { Route } from './../route';
import * as About from './about';

export const AboutRoute =  new Route({
  path: '/about',
  exact: true,
  component: About.default.WrappedComponent
});