import { Route } from './../route';
import * as Main from './main';

export const MainRoute =  new Route({
  path: '/',
  exact: true,
  component: Main.default.WrappedComponent
});