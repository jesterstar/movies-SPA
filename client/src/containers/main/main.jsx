import React from 'react';
import { connect } from 'react-redux';

import {
  Col,
  Row,
  Container
} from 'mdbreact';

import Header from './../../components/header';
import Footer from './../../components/footer';
import MainSlider from './../../components/mainSlider';
import PopularMovies from './../../components/popularMovies';
import PopularTVs from './../../components/popularTVs';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

export class Main extends React.Component {
  render() {
    return (
      <div id="wrap-content">
        <Container fluid={true} className="content">
          <Row>
            <Col>
              <Header/>
            </Col>
          </Row>
          <Row>
            <Col>
              <MainSlider />
            </Col>
          </Row>
          <Row className="popular-movies">
            <Col>
              <Container>
                <Row>
                  <Col>                
                    <PopularMovies />
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
          <Row className="popular-tvs">
            <Col>
              <Container>
                <Row>
                  <Col>                
                    <PopularTVs />
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
        </Container>
        <Footer/>
      </div>
    );
  }
}

export default connect()(Main);