import { Route } from './../route';
import * as NotFound from './notfound';

export const NotFoundRoute = new Route({
  component: NotFound.default.WrappedComponent,
  path: '*'
});