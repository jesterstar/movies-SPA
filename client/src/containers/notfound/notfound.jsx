import * as React from 'react';
import { Button, Container, Row, Col } from 'mdbreact';
import { connect } from 'react-redux';

export class NotFound extends React.Component {
  render() {
    return (
      <div className="wrapper page-404__wrapper">
        <Container>
          <Row>
            <Col xs="12" className="text-center page-404__wrapper">
              <div className="page-404">
                {/*<img src={notFoundImage} className="img-fluid page-404__image" alt="404" />*/}
                <h2 className="page-404__title">Page Not Found</h2>
                <p>Sorry, there is nothing to see here :(</p>
                <Button onClick={window.history.goBack} color="warning">Back to previous page</Button>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state){
}

export default connect(
  mapStateToProps
)(NotFound);