import moment from 'moment';

export default class GeneralHelper {
  static randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
  }

  /**
   * Default date format fro App
   */
  static defalutDataFormat(recievedData) {

    /**
     * For example it will returns (May 28, 2014)
     */
    return moment(recievedData).format('MMMM DD, YYYY');
  }
}