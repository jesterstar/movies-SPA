import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';
import 'react-toastify/dist/ReactToastify.min.css';

import './assets/scss/main.css';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import * as routes from './containers';
import Main from './containers/main/main';
import registerServiceWorker from './registerServiceWorker';
import reducer from './reducers';

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk, logger)));
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      {/*{Object.keys(routes).map(x => {*/}
      {/*const route = routes[x];*/}
      {/*return (*/}
      {/*<Route path={route.path} exact={route.exact} component={route.component} key={`key-${route.path}`}/>*/}
      {/*);*/}
      {/*})}*/}
      <Route path="/" component={Main}/>
    </Router>
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();