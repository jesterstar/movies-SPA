import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import getPopularMoviesWithRedux from './popularMovies';
import getPopularTVsWithRedux from './popularTVs';

export default combineReducers({
  routing: routerReducer,
  getPopularMoviesWithRedux,
  getPopularTVsWithRedux
});