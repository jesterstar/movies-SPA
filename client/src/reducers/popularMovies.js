import * as types from './../actions/actionTypes';

const initialState = [];

export default function getPopularMoviesWithRedux(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_POPULAR_MOVIES_REQUEST:
      return state;
    case types.FETCH_POPULAR_MOVIES_SUCCESS:
      return { ...state, popularMovies: action.payload };
    default:
      return state;
  }
}