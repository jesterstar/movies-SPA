import * as types from './../actions/actionTypes';

const initialState = [];

export default function getPopularTVWithRedux(state = initialState, action) {
  switch (action.type) {
    case types.FETCH_POPULAR_TVS_REQUEST:
      return state;
    case types.FETCH_POPULAR_TVS_SUCCESS:
      return { ...state, popularTVs: action.payload };
    default:
      return state;
  }
}