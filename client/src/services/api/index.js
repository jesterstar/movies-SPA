import axios from 'axios';

import { config } from './../../configs';
import ErrorHandler from './../errorHandler';

import GeneralHelper from './../../helpers/general';

/**
 * ApiService
 */
class ApiService {

  /**
   * Method to parse JWT token
   * @param token
   * @returns {any}
   */
  parseJwt(token) {
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }

  /**
   * Get popular movies
   */
  getPopularMovies() {
    return axios(
      {
        method: 'GET',
        baseURL: `${config.api.url}`,
        url: `${config.api.version}${config.api.popular.movies}?api_key=${config.api.key}&language=${config.api.lang}&page=${GeneralHelper.randomInteger(1, 10)}`,
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((res) => {
        if (res.error) throw res;
        return res.data;
      })
      .catch((e) => {
        console.log(e);
        new ErrorHandler(e);
      });
  }

  /**
   * Get popular TVs
   */
  getPopularTVs() {
    return axios(
      {
        method: 'GET',
        baseURL: `${config.api.url}`,
        url: `${config.api.version}${config.api.popular.tv}?api_key=${config.api.key}&language=${config.api.lang}&page=${GeneralHelper.randomInteger(1, 10)}`,
        headers: {
          'Content-Type': 'application/json'
        }
      })
      .then((res) => {
        if (res.error) throw res;
        return res.data;
      })
      .catch((e) => {
        console.log(e);
        new ErrorHandler(e);
      });
  }
}

export default new ApiService();